if (!require("ecoval")) { install.packages("ecoval"); library(ecoval) }

msk.morphol <- msk.morphol.1998.create(language="ZH")

plot(msk.morphol)

attrib.generic <- list(
  EINDOL     = 0,                # c(0,1),
  BREITENVAR = c(1,2,3),
  SOHLMAT    = 4,                # c(1,2,3,4,5),
  SOHLVER    = c(1,4,6),         # c(1,2,3,4,5,6),
  LBUKMAT    = 7,                # c(1,2,3,4,5,6,7),
  LBUKVER    = c(1,4,6),         # c(1,2,3,4,5,6),
  RBUKMAT    = 7,                # c(1,2,3,4,5,6,7),
  RBUKVER    = c(1,4,6),         # c(1,2,3,4,5,6),
  GSBREITE   = c(1,5,16),        # m
  LUFBEBRE   = c(0,7.5,15),      # m
  LUFBEBEW   = c(1,3),           # c(1,2,3),
  RUFBEBRE   = c(0,7.5,15),      # m
  RUFBEBEW   = c(1,3)            # c(1,2,3)
)

attrib <- expand.grid(attrib.generic)
print(paste("Anzahl Kombinationen:",nrow(attrib)))

val <- evaluate(msk.morphol,attrib)

pdf("msk.morphol_all.pdf",width=9,height=6)
ind.sort <- order(val[,1],decreasing=TRUE)
plot(msk.morphol,u=val[ind.sort,],main=1:nrow(val),with.attrib=FALSE)
dev.off()

