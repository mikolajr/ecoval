if (!require("ecoval")) { install.packages("ecoval"); library(ecoval) }

lake.morphol <- lake.morphol.2016.create(language="ZH")

plot(lake.morphol,two.lines=TRUE)

attrib.generic <- list(
  D01 = c("D01.01","D01.02","D01.03","D01.04","D01.05"),
  C06 = c("C06.01","C06.04"),
  E02 = c("E02.01","E02.02","E02.03","E02.04","E02.05"),
  B01 = c("B01.01","B01.02","B01.03","B01.04","B01.05"),
  C03 = c("C03.01","C03.02","C03.03","C03.05","C03.07"),
  D02 = c("D02.01","D02.04")
)

attrib <- expand.grid(attrib.generic)
attrib <- data.frame(attrib,E01="E01.01",C01="C01.01",C02="C02.01",C04="C04.01",C05="C05.01")

val <- evaluate(lake.morphol,attrib)

pdf("lake.morphol_all.pdf",width=15,height=8)
ind.sort <- order(val[,1],decreasing=TRUE)
plot(lake.morphol,u=val[ind.sort,],main=1:nrow(val),with.attrib=FALSE,two.lines=TRUE)
dev.off()
